#!/bin/bash
PATTERN=FAILED
FILE=logoutput.txt
if grep -q -i $PATTERN $FILE;
then
	echo "ERROR: The following tests have failed '$PATTERN':"
    echo -e -c "$(grep $PATTERN $FILE)\n"
    exit 1
elif  grep -q -i FINISHED $FILE;
then
	cat $FILE;
	echo "All tests passed succesfull"
    echo "Exiting..."
    exit 0
else
	cat $FILE;
	echo "Testrun aborted. Unexpected error"
    echo "Exiting..."
    exit 1
fi